﻿using ScheduleUpdater;
using ScheduleUpdater.DataModel;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Background;

namespace BackgroundTileUpdater
{
    public sealed class TileUpdater : IBackgroundTask
    {
        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            // Get a deferral, to prevent the task from closing prematurely 
            // while asynchronous code is still running.
            BackgroundTaskDeferral deferral = taskInstance.GetDeferral();

            List<RouteStopLink> links = LtcDataSource2.getTiles();
            foreach(RouteStopLink link in links) {
                try {
                    List<ScheduleItem> Schedule = await SUpdater.getScheduleByStopAndRoute3(link);
                    if(Schedule != null){
                        SUpdater.UpdateSecondaryTile(Schedule, link);
                    }
                } catch { }
            }

            // Inform the system that the task is finished.
            deferral.Complete();
        }
    }
}
