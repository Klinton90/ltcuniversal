﻿using SQLite.Net.Attributes;
using System;

namespace ScheduleUpdater.DataModel
{
    public enum Direction
    {
        East = 1,
        North = 2,
        South = 3,
        West = 4
    }

    public interface ILinkable {
        int StopId {get; set;}

        int RouteId {get; set;}

        Direction Direction {get; set;}
    }

    public interface ILink : ILinkable {
        [PrimaryKey, NotNull]
        new int StopId {get; set;}

        [PrimaryKey, NotNull]
        new int RouteId {get; set;}

        [PrimaryKey, NotNull]
        new Direction Direction {get; set;}

        [NotNull]
        bool IsMajor {get; set;}

        [NotNull]
        bool IsFavorite {get; set;}

        [NotNull]
        bool IsTile {get; set;}
    }

    public class RouteStopLink: ILink
    {
        public int StopId {get;set;}

        public int RouteId {get;set;}

        public Direction Direction {get;set;}

        public bool IsMajor {get;set;}

        public bool IsFavorite {get;set;}

        public bool IsTile {get;set;}
    }

    public class Tile : RouteStopLink, ILink {
        public string Name {get;set;}

        public string RouteName {get;set;}
    }

    public class ScheduledTile: ScheduleData, ILink {
        public int StopId {get;set;}

        public int RouteId {get;set;}

        public Direction Direction {get;set;}

        public bool IsMajor {get;set;}

        public bool IsFavorite {get;set;}

        public bool IsTile {get;set;}

        public string Name {get;set;}
    }
}
