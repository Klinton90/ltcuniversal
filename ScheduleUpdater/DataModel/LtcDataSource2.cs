﻿using LtcUniversal;
using SQLite.Net;
using SQLite.Net.Platform.WinRT;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.Storage;
using Windows.UI.StartScreen;
using Windows.UI.Xaml;
using WPGeneralLib;
using System.Reflection;

namespace ScheduleUpdater.DataModel
{
    public class LtcDataSource2
    {
        #region Constants
        private const string _tableNeedle = "<a class=\"ada\" title=\"";

        private const string _fileExtension = ".json";
        #endregion

        private static SQLiteConnection _db = new SQLiteConnection(new SQLitePlatformWinRT(), Path.Combine(ApplicationData.Current.LocalFolder.Path, Constants.dbName));

        #region Synchronization/Restoration
        public async static Task<Boolean> syncLtcDataAsync() {
            List<RouteDb> routesList = new List<RouteDb>();
            List<Stop> stopsList = new List<Stop>();
            List<RouteStopLink> linksList = new List<RouteStopLink>();

            Stream pageStream = await HttpHelper.GetURLContentsAsync(Constants.ltcUrl + "ada.aspx");
            using(StreamReader pageReader = new StreamReader(pageStream)) {
                string sLine;

                while((sLine = pageReader.ReadLine()) != null) {
                    int posStart = sLine.IndexOf(_tableNeedle);
                    if(posStart >= 0) {
                        posStart += _tableNeedle.Length;
                        string _routeId = sLine.Substring(posStart, 3);
                        if(_routeId.Substring(_routeId.Length - 1) == ",") {
                            _routeId = _routeId.Substring(0, _routeId.Length - 1);
                            posStart += 4;
                        } else {
                            posStart += 5;
                        }
                        int routeId = Convert.ToInt32(_routeId);
                        int posEnd = sLine.IndexOf("\"", posStart);
                        string routeName = sLine.Substring(posStart, posEnd - posStart);

                        string rawRouteData = await HttpHelper.getSome(Constants.ltcUrl + "Scripts/Route" + _routeId + "_trace.js");
                        string[] mapStr = rawRouteData.Split(new string[] { "var mapTrace = " }, StringSplitOptions.RemoveEmptyEntries);
                        routesList.Add(new RouteDb {
                            RouteId = routeId,
                            RouteName = routeName,
                            MapData = mapStr[1]
                        });

                        string rawStopsData = await HttpHelper.getSome(Constants.ltcUrl + "UpdateWebMap.aspx?u=" + _routeId);
                        string[] rawStops = rawStopsData.Split('*');
                        if(rawStops.Length <= 3) {
                            return false;
                            //continue;
                        }

                        _parseStops(rawStops[1], routeId, true, ref stopsList, ref linksList);
                        _parseStops(rawStops[3], routeId, false, ref stopsList, ref linksList);
                    }
                }
            }

            _cleanTables();
            _db.InsertOrReplaceAll(routesList);
            _db.InsertOrReplaceAll(stopsList);
            _db.InsertOrReplaceAll(linksList);
            return true;
        }

        private static void _parseStops(string rawStopsData, int routeId, bool isMajor, ref List<Stop> stopsList, ref List<RouteStopLink> linksList) {
            if(rawStopsData.Length > 0) {
                string[] stops = rawStopsData.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach(string rawStopData in stops) {
                    string[] stopData = rawStopData.Split(new char[] { '|' });
                    int stopId = Convert.ToInt32(stopData[4].Substring(12));

                    Direction direction = new Direction();
                    switch(stopData[3]) {
                        case "NORTHBOUND":
                            direction = Direction.North;
                            break;
                        case "SOUTHBOUND":
                            direction = Direction.South;
                            break;
                        case "EASTBOUND":
                            direction = Direction.East;
                            break;
                        case "WESTBOUND":
                            direction = Direction.West;
                            break;
                        default:
                            break;
                    }

                    RouteStopLink link = new RouteStopLink() {
                        RouteId = routeId,
                        StopId = stopId,
                        Direction = direction,
                        IsMajor = isMajor,
                        IsFavorite = false,
                        IsTile = false
                    };

                    linksList.Add(link);

                    var existingStop = stopsList.Where(s => s.StopId == stopId);
                    if(existingStop.Count() == 0) {
                        Stop stop = new Stop() {
                            StopId = Convert.ToInt32(stopData[4].Substring(12)),
                            Name = stopData[2],
                            Lat = Convert.ToDouble(stopData[0]),
                            Lng = Convert.ToDouble(stopData[1]),
                        };
                        stopsList.Add(stop);
                    }
                }
            }
        }

        public static void restoreLtcData() {
            SQLiteConnection db = new SQLiteConnection(new SQLitePlatformWinRT(), Windows.ApplicationModel.Package.Current.InstalledLocation.Path + "\\" + Constants.dataFolder + "\\" + Constants.dbName);

            List<RouteDb> routesList = db.Table<RouteDb>().ToList();
            List<Stop> stopsList = db.Table<Stop>().ToList();
            List<RouteStopLink> linksList = db.Table<RouteStopLink>().ToList();

            _cleanTables();

            _db.InsertOrReplaceAll(routesList);
            _db.InsertOrReplaceAll(stopsList);
            _db.InsertOrReplaceAll(linksList);
        }

        private static void _cleanTables() {
            _db.DropTable<Stop>();
            _db.DropTable<RouteDb>();
            _db.DropTable<RouteStopLink>();
            _db.CreateTable<Stop>();
            _db.CreateTable<RouteDb>();
            string sql = @"CREATE TABLE RouteStopLink(
                                StopId INTEGER NOT NULL,
                                RouteId INTEGER NOT NULL,
                                Direction INTEGER NOT NULL,
                                IsMajor INTEGER NOT NULL,
                                IsFavorite INTEGER NOT NULL,
                                IsTile INTEGER NOT NULL,
                                PRIMARY KEY (StopId, RouteId, Direction)
                           );";
            SQLiteCommand com1 = _db.CreateCommand(sql);
            com1.ExecuteNonQuery();
            SQLiteCommand com2 = _db.CreateCommand("VACUUM;");
            com2.ExecuteNonQuery();
        }
        #endregion

        #region RouteDb
        public static List<RouteDb> getRoutes() {
            return _db.Table<RouteDb>().ToList();
        }

        public static RouteDb getRouteById(int routeId) {
            RouteDb list = _db.Table<RouteDb>().Where(x => x.RouteId == routeId).FirstOrDefault();
            return list;
        }

        public  static List<RouteDb> getFilteredByIdRoutes(string routeId) {
            var list = _db.Table<RouteDb>().ToList().Where(x => x.RouteId.ToString().Contains(routeId));
            return new List<RouteDb>(list);
        }

        public static List<RouteStopLink> getRoutesByStop(int stopId) {
            return _db.Table<RouteStopLink>()
                .Where(link => link.StopId == stopId)
                .GroupBy(link => link.RouteId)
                .Select(link => link.First()).ToList();
        }

        public static List<Tile> getRoutesWithDeskByStop(int stopId) {
            string sql = @"SELECT t.*, r.RouteName
                           FROM RouteStopLink AS 't'
                           INNER JOIN RouteDb AS 'r' ON r.RouteId == t.RouteId
                           WHERE t.StopId == ?
                           GROUP BY t.RouteId;";
            return _db.Query<Tile>(sql, stopId).ToList();
        }
        #endregion

        #region Tile
        public static List<RouteStopLink> getTiles() {
            var list = _db.Table<RouteStopLink>().Where(i => i.IsTile);
            return new List<RouteStopLink>(list);
        }

        public static bool isTile(ILink link) {
            RouteStopLink item = _db.Table<RouteStopLink>().FirstOrDefault(r => r.StopId == link.StopId && r.RouteId == link.RouteId && r.Direction == link.Direction && r.IsTile);
            if(item != null) {
                return true;
            }
            return false;
        }

        public static void toogleFavorite(ILink selRoute, bool value) {
            if(selRoute.IsFavorite != value) {
                selRoute.IsFavorite = value;
                LtcDataSource2.updateLink(selRoute, Constants.linkTableName);

                if(!selRoute.IsFavorite) {
                    CommonFunctionsClass.ShowMessage("Stop has been deleted from Favorites");
                } else {
                    CommonFunctionsClass.ShowMessage("Stop has been added to Favorites");
                }
            }
        }

        public static async Task toogleTile(ILink selRoute, bool value) {
            if(selRoute.IsTile != value) {

                selRoute.IsTile = value;
                LtcDataSource2.updateLink(selRoute, Constants.linkTableName);
                string navParam = SUpdater.getNavStringForStopPage(selRoute);

                if(!selRoute.IsTile) {
                    SecondaryTile secondaryTile = new SecondaryTile(navParam);
                    await secondaryTile.RequestDeleteAsync();

                    CommonFunctionsClass.ShowMessage("Tile has been deleted");
                } else {
                    Type t = Application.Current.GetType();
                    Type[] p = new Type[] { selRoute.GetType() };
                    MethodInfo m = t.GetRuntimeMethod("canAddTile", p);
                    object[] pp = new object[] { selRoute };
                    bool canAddTile = (bool)m.Invoke(Application.Current, pp);

                    if(canAddTile) {
                        string stopName = "Bus:" + selRoute.RouteId + " Stop:" + selRoute.StopId;
                        Uri mediumLogo = new Uri(Constants.Logo150x150);
                        Uri wideLogo = new Uri(Constants.Logo310x150);
                        SecondaryTile secondaryTile = new SecondaryTile(navParam, stopName, navParam, mediumLogo, TileSize.Square150x150);
                        secondaryTile.VisualElements.Wide310x150Logo = wideLogo;
                        secondaryTile.VisualElements.ShowNameOnSquare150x150Logo = true;
                        secondaryTile.VisualElements.ShowNameOnWide310x150Logo = true;

                        CommonFunctionsClass.ShowMessage("New Tile has been created");
                        await secondaryTile.RequestCreateAsync();
                    }
                }
            }
        }
        #endregion

        #region Favorites
        public static List<Tile> getFavorites() {
            string sql = @"SELECT t.*, s.Name
                           FROM RouteStopLink AS 't'
                           INNER JOIN Stop AS 's' ON s.StopId == t.StopId
                           WHERE t.IsTile == 1 OR t.IsFavorite == 1;";
            return _db.Query<Tile>(sql);
        }

        public static bool isFavorite(ILink link) {
            RouteStopLink item = _db.Table<RouteStopLink>().FirstOrDefault(r => r.StopId == link.StopId && r.RouteId == link.RouteId && r.Direction == link.Direction && r.IsFavorite);
            if(item != null) {
                return true;
            }
            return false;
        }
        #endregion

        #region Stop
        public static Stop getStopById(int stopId) {
            return _db.Table<Stop>().Where(x => x.StopId == stopId).FirstOrDefault();
        }

        public static ObservableCollection<Stop> findStopsByName(string stopName) {
            var list = from stop in _db.Table<Stop>()
                       where stop.Name.ToLower().Contains(stopName.ToLower())
                       orderby stop.Name ascending
                       select stop;
            return new ObservableCollection<Stop>(list);
        }

        public static List<Stop> getStopsByRouteDir(int routeId, Direction direction) {
            string sql = @"SELECT s.*
                           FROM Stop AS 's'
                           INNER JOIN RouteStopLink AS 'rst' ON s.StopId = rst.StopId
                           WHERE rst.RouteId = ? AND rst.Direction = ?
                           ORDER BY s.Name ASC;";
            return _db.Query<Stop>(sql, routeId, direction);
        }

        public static List<Stop> getStopsByRouteFilteredById(int routeId, Direction direction, string stop) {
            List<Stop> list = LtcDataSource2.getStopsByRouteDir(routeId, direction);
            return new List<Stop>(list.Where(x => x.StopId.ToString().Contains(stop)));
        }

        public static List<ExtendedStop> getStopsForMap(int routeId) {
            string sql = @"SELECT s.StopId, s.Name, s.Lat, s.Lng, rst.IsMajor, rst.Direction
                           FROM Stop AS 's'
                           INNER JOIN RouteStopLink AS 'rst' ON s.StopId = rst.StopId
                           WHERE rst.RouteId = ?;";
            return _db.Query<ExtendedStop>(sql, routeId);
        }

        public static List<ExtendedStop> getNearestStops(Geocoordinate point) {
            var list = _db.Table<Stop>().ToList()
                .OrderBy(stop => Math.Pow((stop.Lat - point.Point.Position.Latitude), 2) + Math.Pow((stop.Lng - point.Point.Position.Longitude), 2)).Take(20)
                .Select(x => new ExtendedStop() {
                    StopId = x.StopId,
                    Name = x.Name,
                    Lat = x.Lat,
                    Lng = x.Lng
                });
            return new List<ExtendedStop>(list);
        }
        #endregion

        #region Direction
        public static List<Direction> getDirectionsByRoute(int routeId) {
            var list = from link in _db.Table<RouteStopLink>()
                       where link.RouteId == routeId
                       select link.Direction;
            return new List<Direction>(list.Distinct());
        }

        public static Direction getDirectionByRouteStop(int routeId, int stopId) {
            return _db.Table<RouteStopLink>().FirstOrDefault(l => l.StopId == stopId && l.RouteId == routeId).Direction;
        }
        #endregion

        #region Links
        public static void updateLink(ILink link, String tableName = "") {
            TableMapping map = _db.GetMapping<ILink>();
            var cols = (from c in map.Columns
                        where c.IsPK != true && c.Name != "Name"
                        select c).ToList();
            var pks = from c in map.Columns
                      where c.IsPK == true && c.Name != "Name"
                      select c;

            var colsStr = cols.Select(c => "\"" + c.Name + "\" = ? ");
            var pksStr = pks.Select(c => "\"" + c.Name + "\" = ?");
            string sql = String.Format(
                   "UPDATE {0} SET {1} WHERE {2}",
                   tableName == "" ? map.TableName : tableName,
                   String.Join(",", colsStr.ToArray()),
                   String.Join(" AND ", pksStr.ToArray())
            );

            cols.AddRange(pks);
            var vals = cols.Select(c => c.GetValue(link));
            _db.Execute(sql, vals.ToArray());
        }
        #endregion
    }
}
