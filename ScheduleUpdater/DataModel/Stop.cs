﻿using SQLite.Net.Attributes;
using System;

namespace ScheduleUpdater.DataModel
{
    public class Stop
    {
        [PrimaryKey]
        public int StopId {
            get;
            set;
        }

        [Indexed, MaxLength(255), NotNull]
        public string Name {
            get;
            set;
        }

        [NotNull]
        public double Lat {
            get;
            set;
        }

        [NotNull]
        public double Lng {
            get;
            set;
        }
    }

    public class ExtendedStop : Stop
    {
        private int _routeId;

        public int RouteId {
            get {
                return _routeId;
            }
            set {
                _routeId = value;
            }
        }

        private Direction _direction;

        public Direction Direction {
            get {
                return _direction;
            }
            set {
                _direction = value;
            }
        }

        private bool _isMajor;

        public bool IsMajor {
            get {
                return _isMajor;
            }
            set {
                _isMajor = value;
            }
        }
    }
}
