﻿using SQLite.Net.Attributes;
using System;

namespace ScheduleUpdater.DataModel
{
    public class RouteDb
    {
        [PrimaryKey]
        public int RouteId {
            get;
            set;
        }

        [Indexed, MaxLength(255), NotNull]
        public string RouteName {
            get;
            set;
        }

        [NotNull]
        public string MapData {
            get;
            set;
        }
    }
}
