﻿using System;

namespace ScheduleUpdater.DataModel
{
    public class ScheduleData {
        public TimeSpan? Time {get; set;}

        public string TimeString {
            get {
                if(Time != null) {
                    return "@" + ((TimeSpan)Time).ToString().Substring(0, 5);
                } else {
                    return String.Empty;
                }
            }
        }

        public int TimeDif {
            get {
                if(Time != null){
                    TimeSpan tempTime = (TimeSpan)Time;
                    TimeSpan dif = tempTime - DateTime.Now.TimeOfDay;
                    int minutes = (int)dif.TotalMinutes;
                    if(minutes < 0) {
                        if(tempTime.Hours == 0) {
                            minutes += 1440;
                        } else {
                            return 0;
                        }
                    }
                    return minutes;
                }else{
                    return 0;
                }
            }
        }
    }

    public class ScheduleItem: ScheduleData, ILinkable {
        public string DirectionText {get; set;}

        public int RouteId {get; set;}

        public int StopId {get; set;}

        public Direction Direction {get;set;}
    }
}
