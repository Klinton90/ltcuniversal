﻿using HtmlAgilityPack;
using LtcUniversal;
using NotificationsExtensions.TileContent;
using ScheduleUpdater.DataModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Windows.UI.Notifications;
using WPGeneralLib;

namespace ScheduleUpdater {
    public class SUpdater
    {
        private const string _header = "Next Busses:";

        private const string AltColor = "Gray image";

        #region Schedule Update
        public static async Task<List<ScheduleItem>> getScheduleByStopAndRoute(ILinkable link) {
            List<ScheduleItem> schedule = new List<ScheduleItem>();

            using(Stream stopStream = await HttpHelper.GetURLContentsAsync(Constants.ltcUrl + "UpdateWebMap.aspx?u=" + link.RouteId.ToString())) {
                using(StreamReader stopReader = new StreamReader(stopStream)) {
                    string rawStopsData = await stopReader.ReadToEndAsync();
                    string[] rawStops = rawStopsData.Split('*');
                    if(rawStops.Count() <= 1){
                        return null;
                    }

                    schedule = _parseStopsForSchedule(rawStops[1], link);
                    if(schedule.Count == 0) {
                        schedule = _parseStopsForSchedule(rawStops[3], link);
                    }
                }
            }

            return schedule;
        }

        private static List<ScheduleItem> _parseStopsForSchedule(string rawStopsData, ILinkable link) {
            List<ScheduleItem> result = new List<ScheduleItem>();

            string[] stops = rawStopsData.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            foreach(string rawStopData in stops) {
                string[] stopData = rawStopData.Split(new char[] { '|' });
                int _stopId = Convert.ToInt32(stopData[4].Substring(12));

                if(_stopId == link.StopId) {
                    string[] rawScheduleData = stopData[5].Split(new string[] { "<br>" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach(string scheduleData in rawScheduleData) {
                        string[] temp = scheduleData.Split(new string[] { " TO " }, StringSplitOptions.RemoveEmptyEntries);
                        result.Add(new ScheduleItem() {
                            StopId = link.StopId,
                            RouteId = link.RouteId,
                            Direction = link.Direction,
                            Time = Convert.ToDateTime(temp.First()).TimeOfDay,
                            DirectionText = temp.Last()
                        });
                    }
                }
            }

            return result;
        }

        public static async Task<List<ScheduleItem>> getScheduleByStopAndRoute2(ILinkable link) {
            List<ScheduleItem> schedule = new List<ScheduleItem>();

            int direction = (int)link.Direction;
            String path = Constants.ltcUrl + "ada.aspx?r=" + link.RouteId.ToString("D2") + "&d=" + direction.ToString() + "&s=" + link.StopId.ToString();
            using(Stream stopStream = await HttpHelper.GetURLContentsAsync(path)) {
                using(StreamReader stopReader = new StreamReader(stopStream)) {
                    string rawStopsData = await stopReader.ReadToEndAsync();
                    HtmlDocument result = new HtmlDocument();
                    result.LoadHtml(rawStopsData);

                    HtmlNode table = result.DocumentNode.Descendants()
                        .Where(x => x.Name == "table" && x.Attributes["id"] != null && x.Attributes["id"].Value.Contains("tblADA"))
                        .FirstOrDefault();
                    if(table == null){
                        //Note, I disabled second shedule update, as it makes app slow.
                        //return null;
                        return schedule;
                    }
                    List<HtmlNode> tableNodes = table.Descendants()
                        .Where(x => x.Name == "tr" && x.Attributes["class"] == null)
                        .ToList();
                    schedule = tableNodes.Select(node => {
                        string time = node.Descendants()
                            .Where(y => y.Name == "a")
                            .Select(y => y.InnerText.Replace(".", String.Empty)).FirstOrDefault();
                        string dir = node.Descendants()
                            .Where(y => y.Name == "td" && y.Attributes["align"] != null && y.Attributes["align"].Value.Contains("left"))
                            .Select(y => y.InnerText).FirstOrDefault();
                        int dummy;
                        if(int.TryParse(time.Substring(0, 1), out dummy)) {
                            return new ScheduleItem() {
                                StopId = link.StopId,
                                RouteId = link.RouteId,
                                Direction = link.Direction,
                                Time = Convert.ToDateTime(time).TimeOfDay,
                                DirectionText = dir
                            };
                        } else {
                            return null;
                        }
                    }).Where(node => node != null).ToList();
                }
            }

            return schedule;
        }

        public static async Task<List<ScheduleItem>> getScheduleByStopAndRoute3(ILinkable link) {
            List<ScheduleItem> result = new List<ScheduleItem>();
            result = await getScheduleByStopAndRoute2(link);
            if(result == null){
                result = await getScheduleByStopAndRoute(link);
            }
            return result;
        }
        #endregion

        #region Tile Funcs

        public static void UpdateSecondaryTile(IEnumerable<ScheduleData> Schedule, ILinkable link) {
            String navString = getNavStringForStopPage(link);
            TileUpdater updater = TileUpdateManager.CreateTileUpdaterForSecondaryTile(navString);
            
            if(updater.Setting == NotificationSetting.Enabled) {
                ITileSquare71x71Image tile71x71 = TileContentFactory.CreateTileSquare71x71Image();
                tile71x71.Image.Src = Constants.Logo71x71;
                tile71x71.Image.Alt = AltColor;

                var tileWithImage = Windows.Storage.ApplicationData.Current.LocalSettings.Values["tileWithImage"];
                if(tileWithImage != null && (bool)tileWithImage) {
                    ITileWide310x150PeekImage02 tile310x150 = TileContentFactory.CreateTileWide310x150PeekImage02();
                    tile310x150.Image.Src = Constants.Logo310x150;
                    tile310x150.Image.Alt = AltColor;
                    tile310x150.TextHeading.Text = _header;

                    ITileSquare150x150PeekImageAndText01 tile150x150 = TileContentFactory.CreateTileSquare150x150PeekImageAndText01();
                    tile150x150.Image.Src = Constants.Logo150x150;
                    tile150x150.Image.Alt = AltColor;
                    tile150x150.TextHeading.Text = _header;
                    
                    if(Schedule.Count() > 0) {
                        tile310x150.TextBody1.Text = Schedule.ElementAtOrDefault(0) != null ? Schedule.ElementAt(0).TimeString : String.Empty;
                        tile310x150.TextBody2.Text = Schedule.ElementAtOrDefault(1) != null ? Schedule.ElementAt(1).TimeString : String.Empty;
                        tile310x150.TextBody3.Text = Schedule.ElementAtOrDefault(2) != null ? Schedule.ElementAt(2).TimeString : String.Empty;

                        tile150x150.TextBody1.Text = Schedule.ElementAtOrDefault(0) != null ? Schedule.ElementAt(0).TimeString : String.Empty;
                        tile150x150.TextBody2.Text = Schedule.ElementAtOrDefault(1) != null ? Schedule.ElementAt(1).TimeString : String.Empty;
                        tile150x150.TextBody3.Text = Schedule.ElementAtOrDefault(2) != null ? Schedule.ElementAt(2).TimeString : String.Empty;
                    } else {
                        tile310x150.TextBody1.Text = "No futher buses found";
                        tile150x150.TextBody1.Text = "No futher buses found";
                    }

                    tile150x150.Square71x71Content = tile71x71;
                    tile310x150.Square150x150Content = tile150x150;

                    updater.Update(tile310x150.CreateNotification());
                } else {
                    ITileWide310x150Text01 tile310x150 = TileContentFactory.CreateTileWide310x150Text01();
                    tile310x150.TextHeading.Text = _header;

                    ITileSquare150x150Text01 tile150x150 = TileContentFactory.CreateTileSquare150x150Text01();
                    tile150x150.TextHeading.Text = _header;

                    if(Schedule.Count() > 0) {
                        tile310x150.TextBody1.Text = Schedule.ElementAtOrDefault(0) != null ? Schedule.ElementAt(0).TimeString : String.Empty;
                        tile310x150.TextBody2.Text = Schedule.ElementAtOrDefault(1) != null ? Schedule.ElementAt(1).TimeString : String.Empty;
                        tile310x150.TextBody3.Text = Schedule.ElementAtOrDefault(2) != null ? Schedule.ElementAt(2).TimeString : String.Empty;

                        tile150x150.TextBody1.Text = Schedule.ElementAtOrDefault(0) != null ? Schedule.ElementAt(0).TimeString : String.Empty;
                        tile150x150.TextBody2.Text = Schedule.ElementAtOrDefault(1) != null ? Schedule.ElementAt(1).TimeString : String.Empty;
                        tile150x150.TextBody3.Text = Schedule.ElementAtOrDefault(2) != null ? Schedule.ElementAt(2).TimeString : String.Empty;
                    } else {
                        tile310x150.TextBody1.Text = "No futher buses found";
                        tile150x150.TextBody1.Text = "No futher buses found";
                    }

                    tile150x150.Square71x71Content = tile71x71;
                    tile310x150.Square150x150Content = tile150x150;

                    updater.Update(tile310x150.CreateNotification());
                }
            } else {
                clearSecondaryTile(navString);
            }
        }

        public static void clearSecondaryTile(string navString){
            TileUpdateManager.CreateTileUpdaterForSecondaryTile(navString).Clear();
        }
        #endregion

        #region NavString processing
        public static string getNavStringForStopPage(ILinkable link) {
            return link.StopId.ToString() + "_" + link.RouteId.ToString() + "_" + link.Direction.ToString();
        }
        #endregion
    }
}
