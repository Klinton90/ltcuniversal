﻿using System;
using Windows.ApplicationModel.Background;

namespace LtcUniversal
{
    public class Constants
    {
        #region TileUpdater setup
        public static readonly string taskName = "BackgroundTileUpdater";
        public static readonly string taskEntryPoint = "BackgroundTileUpdater.TileUpdater";
        public static readonly IBackgroundTrigger[] triggers = new IBackgroundTrigger[] { new TimeTrigger(15, false) };
        public static readonly IBackgroundCondition[] conditions = new IBackgroundCondition[] { new SystemCondition(SystemConditionType.InternetAvailable) };
        #endregion

        #region Advertisement opts
        public static readonly string AdUnitId = "XXXXXXXX";
        public static readonly string AdApplicationId = "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX";
        public static readonly int AdHeight = 50;
        public static readonly int AdWidth = 320;
        public static readonly int maxTiles = 5;
        #endregion

        #region Logo images
        public static readonly string Logo150x150 = "ms-appx:///Assets/Logo.scale-240.png";
        public static readonly string Logo71x71 = "ms-appx:///Assets/Square71x71Logo.scale-240.png";
        public static readonly string Logo310x150 = "ms-appx:///Assets/WideLogo.scale-240.png";
        #endregion

        #region Button Brushes
        public static readonly string CloseBrush = "ms-appx:///Assets/Close.png";
        #endregion

        #region Deprecated. Store for version handling
        public static readonly string routesFileName = "routes";
        public static readonly string stopsFileName = "stops";
        public static readonly string linksFileName = "links";
        public static readonly string favoritesFileName = "favorites";
        public static readonly string tilesFileName = "tiles";
        #endregion

        #region DB setup
        public static readonly string dbName = "LtcUniversal.db";
        public static readonly string dataFolder = "Assets";
        public static readonly string linkTableName = "RouteStopLink";
        public static readonly string simFileName = "Simulation.xml";
        #endregion

        #region Connection setup
        public static readonly string ltcUrl = "http://www.ltconline.ca/WebWatch/";
        public enum connectionError{
            ok = 0,
            siteNotAvailable = 1,
            connectionFail = 2,
            noData = 3,
        }
        #endregion
    }
}
