﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Storage;

namespace WPGeneralLib
{
    public class WPFileOperations
    {
        public WPFileOperations() {
        }

        public WPFileOperations(string folderName):this() {
            FolderName = folderName;
        }

        public WPFileOperations(string folderName, string fileExtenstion) : this(folderName) {
            FileExtension = fileExtenstion;
        }

        #region Read/Write to existing file
        public string FolderName {
            get;
            set;
        }

        public string FileExtension {
            get;
            set;
        }

        public async Task<StorageFile> getFileAsync(string fileName) {
            if(FolderName == null || FolderName == String.Empty){
                throw new Exception("Folder Name not specified");
            }
            StorageFile file = await (await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync(FolderName)).GetFileAsync(fileName);
            return file;
        }

        public async Task writeJsonToFileAsync<T>(string fileName, IEnumerable<T> data) {
            StorageFile file = await getFileAsync(fileName);
            
            var serializer = new DataContractJsonSerializer(data.GetType());
            await truncateFileAsync(fileName);
            using(Stream stream = await file.OpenStreamForWriteAsync()) {
                serializer.WriteObject(stream, data);
            }
        }

        public async Task<JsonArray> readJsonFromFileAsync(string fileName) {
            JsonArray jsonArray = new JsonArray();
            StorageFile file = await getFileAsync(fileName);

            string data = await FileIO.ReadTextAsync(file);

            if(data.Length > 0) {
                jsonArray = JsonArray.Parse(data);
            }

            return jsonArray;
        }

        public async Task copyFileContentsAsync(string readFileName, string writeFileName) {
            StorageFile readFile = await getFileAsync(readFileName);
            StorageFolder destinationFolder = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync(FolderName);
            await readFile.CopyAsync(destinationFolder, writeFileName, NameCollisionOption.ReplaceExisting);
        }

        public async Task truncateFileAsync(string fileName) {
            StorageFile file = await getFileAsync(fileName);

            using(Stream stream = await file.OpenStreamForWriteAsync()) {
                stream.SetLength(0);
                await stream.WriteAsync(new byte[0], 0, 0);
            }
        }
        #endregion

        #region Write to Local Storage
        public async Task writeJsonToLocalStorageFileAsync<T>(string fileName, IEnumerable<T> data) {
            var serializer = new DataContractJsonSerializer(data.GetType());
            using(Stream stream = await ApplicationData.Current.LocalFolder.OpenStreamForWriteAsync(fileName + FileExtension, CreationCollisionOption.ReplaceExisting)) {
                serializer.WriteObject(stream, data);
            }
        }

        public async Task truncateLocalStorageFileAsync(string fileName) {
            using(Stream stream = await ApplicationData.Current.LocalFolder.OpenStreamForWriteAsync(fileName + FileExtension, CreationCollisionOption.ReplaceExisting)) {
                stream.SetLength(0);
                await stream.WriteAsync(new byte[0], 0, 0);
            }
        }

        public async Task<JsonArray> readJsonFromLocalStorageFileAsync(string fileName, bool create = false) {
            JsonArray jsonArray = new JsonArray();

            bool exists = await ApplicationData.Current.LocalFolder.FileExists(fileName + FileExtension);
            if(exists){
                StorageFile file = await ApplicationData.Current.LocalFolder.GetFileAsync(fileName + FileExtension);
                string data = await FileIO.ReadTextAsync(file);

                if(data.Length > 0) {
                    jsonArray = JsonArray.Parse(data);
                }
            } else if(create) {
                await ApplicationData.Current.LocalFolder.CreateFileAsync(fileName + FileExtension, CreationCollisionOption.ReplaceExisting);
            }

            return jsonArray;
        }

        public async Task copyFileToLocalStorageAsync(string readFileName, string writeFileName) {
            StorageFile readFile = await getFileAsync(readFileName);
            await readFile.CopyAsync(ApplicationData.Current.LocalFolder, writeFileName, NameCollisionOption.ReplaceExisting);
        }
        #endregion
    }

    public static class FileExtensions
    {
        public static async Task<bool> FileExists(this StorageFolder folder, string fileName) {
            try {
                StorageFile file = await folder.GetFileAsync(fileName);
            } catch {
                return false;
            }
            return true;
        }

        public static async Task<bool> FileExist2(this StorageFolder folder, string fileName) {
            return (await folder.GetFilesAsync()).Any(x => x.Name.Equals(fileName));
        }
    }
}
