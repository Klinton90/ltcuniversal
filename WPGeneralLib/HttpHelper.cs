﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WPGeneralLib
{
    public class HttpHelper
    {
        //static string customHeaderName = "User-Agent";
        //static string customHeaderValue = "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)";
        public static async Task<Stream> GetURLContentsAsync(string url) {
            Stream responseStream;
            WebRequest request = WebRequest.Create(url);
            if(request.Headers == null) {
                request.Headers = new WebHeaderCollection();
            }
            request.Headers[HttpRequestHeader.IfModifiedSince] = DateTime.UtcNow.ToString();
            request.UseDefaultCredentials = true;

            WebResponse response = await request.GetResponseAsync();
            using(response) {
                responseStream = response.GetResponseStream();
            }

            return responseStream;
        }

        public static async Task<string> getSome(string url){
            HttpClient http = new HttpClient();
            var response = await http.GetByteArrayAsync(url);
            string source = Encoding.GetEncoding("utf-8").GetString(response, 0, response.Length - 1);
            source = WebUtility.HtmlDecode(source);
            return source;
        }
    }
}
