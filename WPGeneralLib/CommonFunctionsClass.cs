﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using NotificationsExtensions.ToastContent;
using Windows.UI.Notifications;

namespace WPGeneralLib
{
    public class CommonFunctionsClass
    {
        public static DependencyObject FindChildControl<T>(DependencyObject control, string ctrlName) {
            int childNumber = VisualTreeHelper.GetChildrenCount(control);
            for(int i = 0; i < childNumber; i++) {
                DependencyObject child = VisualTreeHelper.GetChild(control, i);
                FrameworkElement fe = child as FrameworkElement;
                // Not a framework element or is null
                if(fe == null)
                    return null;

                if(child is T && fe.Name == ctrlName) {
                    // Found the control so return
                    return child;
                } else {
                    // Not found it - search children
                    DependencyObject nextLevel = FindChildControl<T>(child, ctrlName);
                    if(nextLevel != null)
                        return nextLevel;
                }
            }
            return null;
        }

        public static void ShowMessage(string message) {
            IToastText01 toastTemplate = ToastContentFactory.CreateToastText01();
            toastTemplate.TextBodyWrap.Text = message;
            ToastNotifier notifier = ToastNotificationManager.CreateToastNotifier();
            ToastNotification toast = toastTemplate.CreateNotification();
            toast.ExpirationTime = new DateTimeOffset(DateTime.Now.AddSeconds(1));
            notifier.Show(toast);
        }
    }

    public static class WindowExtensions
    {
        //Common Notifier for PropertyChanges
        public static bool ChangeAndNotify<T>(this PropertyChangedEventHandler handler, ref T field, T value, Expression<Func<T>> memberExpression) {
            if(memberExpression == null) {
                throw new ArgumentNullException("memberExpression");
            }
            var body = memberExpression.Body as MemberExpression;
            if(body == null) {
                throw new ArgumentException("Lambda must return a property.");
            }
            if(EqualityComparer<T>.Default.Equals(field, value)) {
                return false;
            }
            field = value;

            var vmExpression = body.Expression as ConstantExpression;
            if(vmExpression != null) {
                LambdaExpression lambda = System.Linq.Expressions.Expression.Lambda(vmExpression);
                Delegate vmFunc = lambda.Compile();
                object sender = vmFunc.DynamicInvoke();
                if(handler != null) {
                    handler(sender, new PropertyChangedEventArgs(body.Member.Name));
                }
            }

            return true;
        }
    }
}
