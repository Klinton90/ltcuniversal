﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace WPGeneralLib.Converters
{
    public class BoolToVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language) {
            bool _value = (bool)value;
            bool _invert = parameter != null && parameter.ToString() == "True" ? true : false;
            Visibility ret = Visibility.Visible;
            if(_value) {
                if(_invert){
                    ret = Visibility.Collapsed;
                }else{
                    ret = Visibility.Visible;
                }
            } else {
                if(_invert){
                    ret = Visibility.Visible;
                }else{
                    ret = Visibility.Collapsed;
                }
            }
            return ret;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language) {
            throw new NotImplementedException();
        }
    }
}
