﻿using System;
using Windows.UI.Xaml.Data;

namespace WPGeneralLib.Converters
{
    public class StringFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language) {
            return String.Format((string)parameter, value.ToString());
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language) {
            throw new NotImplementedException();
        }
    }
}
