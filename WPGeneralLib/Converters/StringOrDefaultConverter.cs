﻿using System;
using Windows.UI.Xaml.Data;

namespace WPGeneralLib.Converters {
    public class StringOrDefaultConverter: IValueConverter {
        public object Convert(object value, Type targetType, object parameter, string language) {
            String val = (String)value;
            return val == String.Empty ? (String)parameter : val;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language) {
            throw new NotImplementedException();
        }
    }
}
