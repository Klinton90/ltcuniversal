﻿using System;
using Windows.UI.Xaml.Data;

namespace WPGeneralLib.Converters
{
    public class InvertBool : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language) {
            return value != null ? !((bool)value) : true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language) {
            throw new NotImplementedException();
        }
    }
}