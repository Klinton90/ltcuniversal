﻿using System;

namespace WPGeneralLib
{
    public class WPSettings2
    {
        /// <summary>
        /// Update a setting value for our application. If the setting does not
        /// exist, then add the setting.
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool AddOrUpdateValue(string Key, Object value) {
            bool valueChanged = false;

            if(Windows.Storage.ApplicationData.Current.LocalSettings.Values.ContainsKey(Key)) {
                if(Windows.Storage.ApplicationData.Current.LocalSettings.Values[Key] != value) {
                    Windows.Storage.ApplicationData.Current.LocalSettings.Values[Key] = value;
                    valueChanged = true;
                }
            } else {
                Windows.Storage.ApplicationData.Current.LocalSettings.Values.Add(Key, value);
                valueChanged = true;
            }
            return valueChanged;
        }

        /// <summary>
        /// Get the current value of the setting, or if it is not found, set the 
        /// setting to the default setting.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static T GetValueOrDefault<T>(string Key, T defaultValue) {
            T value;

            if(Windows.Storage.ApplicationData.Current.LocalSettings.Values.ContainsKey(Key)) {
                value = (T)Windows.Storage.ApplicationData.Current.LocalSettings.Values[Key];
                return value;
            } else {
                value = defaultValue;
                return value;
            }
        }
    }
}
