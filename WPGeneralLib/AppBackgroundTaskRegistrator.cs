﻿using System;
using Windows.ApplicationModel.Background;

namespace WPGeneralLib
{
    public class AppBackgroundTaskRegistrator
    {
        public static async void RegisterBackgroundTask(string taskEntryPoint,
                                                        string taskName,
                                                        IBackgroundTrigger[] triggers,
                                                        IBackgroundCondition[] conditions = null) 
        {
            var backgroundAccessStatus = await BackgroundExecutionManager.RequestAccessAsync();
            if(backgroundAccessStatus == BackgroundAccessStatus.AllowedMayUseActiveRealTimeConnectivity ||
                backgroundAccessStatus == BackgroundAccessStatus.AllowedWithAlwaysOnRealTimeConnectivity) {

                BackgroundTaskBuilder taskBuilder = new BackgroundTaskBuilder();
                taskBuilder.Name = taskName;
                taskBuilder.TaskEntryPoint = taskEntryPoint;
                foreach(IBackgroundTrigger trigger in triggers) {
                    taskBuilder.SetTrigger(trigger);
                }
                if(conditions != null) {
                    foreach(IBackgroundCondition condition in conditions) {
                        taskBuilder.AddCondition(condition);
                    }
                    taskBuilder.CancelOnConditionLoss = true;
                }
                var registration = taskBuilder.Register();
            }
        }

        public static void UnregisterBackgroundTask(string taskName) {
            foreach(var task in BackgroundTaskRegistration.AllTasks) {
                if(task.Value.Name == taskName) {
                    task.Value.Unregister(true);
                }
            }
        }

        public static bool IsRegistered(string taskName) {
            foreach(var task in BackgroundTaskRegistration.AllTasks) {
                if(task.Value.Name == taskName) {
                    return true;
                }
            }
            return false;
        }
    }
}
