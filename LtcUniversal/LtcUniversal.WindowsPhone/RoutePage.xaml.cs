﻿using ScheduleUpdater.DataModel;
using LtcUniversal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using Windows.Graphics.Display;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using WPGeneralLib;
using ScheduleUpdater;

namespace LtcUniversal {
    public sealed partial class RoutePage : Page
    {
        #region Standard params
        private readonly NavigationHelper navigationHelper;
        public NavigationHelper NavigationHelper {
            get {
                return this.navigationHelper;
            }
        }

        private readonly ObservableDictionary _viewModel = new ObservableDictionary();
        public ObservableDictionary ViewModel {
            get {
                return this._viewModel;
            }
        }
        #endregion

        public RoutePage()
        {
            this.InitializeComponent();
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait;

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            this.ViewModel["ShowProcessing"] = false;

            if(!(Application.Current as App).checkIAP("noAds")) {
                var adv = AdCreator.createAdControl();
                Grid.SetRow(adv, 1);
                LayoutRoot.Children.Add(adv);
            }

            RouteDb route = LtcDataSource2.getRouteById(Convert.ToInt32(e.NavigationParameter));
            this.ViewModel["Route"] = route;

            List<Direction> directions = LtcDataSource2.getDirectionsByRoute(route.RouteId);
            if(directions.Count > 0){
                List<Stop> Stops_Dir0 = LtcDataSource2.getStopsByRouteDir(route.RouteId, directions.First());
                List<Stop> Stops_Dir1 = LtcDataSource2.getStopsByRouteDir(route.RouteId, directions.Last());

                this.ViewModel["Directions"] = directions;
                this.ViewModel["Stops_Dir0"] = Stops_Dir0;
                this.ViewModel["Stops_Dir1"] = Stops_Dir1;
                this.ViewModel["HasError"] = false;
            } else {
                this.ViewModel["HasError"] = true;
            }
        }

        private void Stop_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.ViewModel["ShowProcessing"] = true;
            int stopId = ((Stop)e.ClickedItem).StopId;

            if(!Frame.Navigate(typeof(StopPage), stopId.ToString())) {
                throw new Exception("NavigationFailedExceptionMessage");
            }
        }

        private void ShowMap_Click(object sender, RoutedEventArgs e) {
            if(!Frame.Navigate(typeof(MapPage), ((RouteDb)this.ViewModel["Route"]).RouteId)) {
                throw new Exception("NavigationFailedExceptionMessage");
            }
        }

        private void SearchStopValue_TextChanged(object sender, TextChangedEventArgs e) {
            RouteDb route = (RouteDb)this.ViewModel["Route"];
            string section = (string)((TextBox)sender).Tag;
            List<Direction> directions = (List<Direction>)this.ViewModel["Directions"];
            this.ViewModel["Stops_Dir" + section] = LtcDataSource2.getStopsByRouteFilteredById(route.RouteId, directions[Convert.ToInt32(section)], ((TextBox)sender).Text);
        }

        #region NavigationHelper registration

        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e) {
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion
    }
}