﻿using ScheduleUpdater.DataModel;
using LtcUniversal.Common;
using ScheduleUpdater;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using Windows.ApplicationModel.Resources;
using Windows.ApplicationModel.Store;
using Windows.Graphics.Display;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using WPGeneralLib;
using Microsoft.Advertising.WinRT.UI;

namespace LtcUniversal {
    public sealed partial class TopPage : Page
    {
        #region Standard params
        //private readonly ResourceLoader resourceLoader = ResourceLoader.GetForCurrentView("Resources");

        private readonly NavigationHelper navigationHelper;
        public NavigationHelper NavigationHelper {
            get {
                return this.navigationHelper;
            }
        }

        private readonly ObservableDictionary _viewModel = new ObservableDictionary();
        public ObservableDictionary ViewModel {
            get {
                return this._viewModel;
            }
        }
        #endregion

        static List<ScheduleItem> oldSchedule = new List<ScheduleItem>();

        private bool _searchById = true;

        private bool _canUpdateTile = false;

        private bool _cancelToken;

        private AdControl _adControl;

        public TopPage()
        {
            ViewModel["isReady"] = false;
            this.InitializeComponent();
            ViewModel["isReady"] = true;
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait;

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        private bool _fillProducts() {
            App app = Application.Current as App;
            bool noAds = app.checkIAP("noAds");
            this.ViewModel["NoAds"] = !noAds;
            this.ViewModel["MoreTiles"] = !app.checkIAP("tiles");
            if(noAds && _adControl != null) {
                _adControl.Visibility = Visibility.Collapsed;
            }
            return noAds;
        }

        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            Frame.BackStack.Clear();

            int adHeight = 0;
            if(!_fillProducts()) {
                _adControl = AdCreator.createAdControl();
                Grid.SetRow(_adControl, 1);
                LayoutRoot.Children.Add(_adControl);
                adHeight = Constants.AdHeight;
            }

            var tileWithImage = Windows.Storage.ApplicationData.Current.LocalSettings.Values["tileWithImage"];
            this.ViewModel["tileWithImage"] = tileWithImage != null ? (bool)tileWithImage : false;
            ViewModel["ConnectionWarning"] = "Press 'Update' to get times";
            this.ViewModel["SyncReady"] = true;
            this.ViewModel["ShowProcessing"] = false;
            this.ViewModel["SearchById"] = true;
            this.ViewModel["AppHeight"] = Window.Current.Bounds.Height - Windows.UI.ViewManagement.StatusBar.GetForCurrentView().OccludedRect.Height - adHeight;
            this.ViewModel["AppWidth"] = Window.Current.Bounds.Width;
            this.ViewModel["RoutesList"] = LtcDataSource2.getRoutes();
            this.ViewModel["FoundStops"] = new ObservableCollection<Stop>();
            this.ViewModel["FavoritesList"] = new ObservableCollection<ScheduledTile>(
                from f in LtcDataSource2.getFavorites()
                join s in oldSchedule on new {j1 = f.RouteId, j2 = f.StopId, j3 = f.Direction} equals new {j1 = s.RouteId, j2 = s.StopId, j3 = s.Direction} into ss
                from _s in ss.DefaultIfEmpty()
                select new ScheduledTile {
                    StopId = f.StopId,
                    RouteId = f.RouteId,
                    Name = f.Name,
                    Direction = f.Direction,
                    IsTile = f.IsTile,
                    IsFavorite = f.IsFavorite,
                    IsMajor = f.IsMajor,
                    Time = _s == null ? null : _s.Time
                }
            );

            if(oldSchedule.Count == 0) {
                _updateSchedule();
            }
        }

        #region NavigationHelper registration

        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e) {
            // TODO: Save the unique state of the page here.
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            if(!(bool)ViewModel["isReady"]) {
                _cancelToken = true;
            }
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        #region Synchronization
        private async void RunSync_Click(object sender, RoutedEventArgs e) {
            string StatusText = "";

            this.ViewModel["StatusText"] = "Synchronization in progress";
            this.ViewModel["SyncReady"] = false;
            await Windows.UI.ViewManagement.StatusBar.GetForCurrentView().HideAsync();
            this.ViewModel["ShowProcessing"] = true;

            try {
                bool answer = await LtcDataSource2.syncLtcDataAsync();
                this.ViewModel["RoutesList"] = LtcDataSource2.getRoutes();
                if(!answer) {
                    StatusText = "Some data is missed. Sync was aborted to prevent data damage";
                }
            } catch {
                StatusText = "Can't connect to LTC website. Try again later.";
                LtcDataSource2.restoreLtcData();
            }

            this.ViewModel["ShowProcessing"] = false;
            await Windows.UI.ViewManagement.StatusBar.GetForCurrentView().ShowAsync();
            this.ViewModel["StatusText"] = StatusText;
            this.ViewModel["SyncReady"] = true;
        }

        private async void RunRestore_Click(object sender, RoutedEventArgs e) {
            this.ViewModel["StatusText"] = "Restoration in progress";
            this.ViewModel["SyncReady"] = false;
            await Windows.UI.ViewManagement.StatusBar.GetForCurrentView().HideAsync();
            this.ViewModel["ShowProcessing"] = true;

            LtcDataSource2.restoreLtcData();
            this.ViewModel["RoutesList"] = LtcDataSource2.getRoutes();

            this.ViewModel["ShowProcessing"] = false;
            await Windows.UI.ViewManagement.StatusBar.GetForCurrentView().ShowAsync();
            this.ViewModel["StatusText"] = "";
            this.ViewModel["SyncReady"] = true;
        }
        #endregion

        #region Navigation to other pages
        private void Route_ItemClick(object sender, ItemClickEventArgs e) {
            this.ViewModel["ShowProcessing"] = true;

            if(!Frame.Navigate(typeof(RoutePage), ((RouteDb)e.ClickedItem).RouteId)) {
                throw new Exception("NavigationFailedExceptionMessage");
            }
        }

        private void Stop_ItemClick(object sender, ItemClickEventArgs e) {
            this.ViewModel["ShowProcessing"] = true;
            int stopId = (int)e.ClickedItem.GetType().GetRuntimeProperty("StopId").GetValue(e.ClickedItem);

            if(!Frame.Navigate(typeof(StopPage), stopId.ToString())) {
                var resourceLoader = ResourceLoader.GetForCurrentView("Resources");
                throw new Exception(resourceLoader.GetString("NavigationFailedExceptionMessage"));
            }
        }

        private void TestFindMe_Click(object sender, RoutedEventArgs e) {
            if(!Frame.Navigate(typeof(MapPage))) {
                throw new Exception("NavigationFailedExceptionMessage");
            }
        }

        void StopPage_Suspending(object sender, Windows.ApplicationModel.SuspendingEventArgs e) {
            ScheduledTile selRoute = (ScheduledTile)((ToggleSwitch)TileSwitch).DataContext;
            var timeList = from item in oldSchedule
                           where item.RouteId == selRoute.RouteId && item.StopId == selRoute.StopId && item.Direction == selRoute.Direction && item.Time != null
                           select new ScheduleData { Time = item.Time };
            if(timeList.Count() != 0) {
                SUpdater.UpdateSecondaryTile(timeList, selRoute);
            }

            (Application.Current as App).Suspending -= StopPage_Suspending;
        }

        private void StopPage_Resuming(object sender, object e) {
            ScheduledTile selRoute = (ScheduledTile)((ToggleSwitch)TileSwitch).DataContext;
            ViewModel["canAddTile"] = (Application.Current as App).canAddTile(selRoute);
            (Application.Current as App).Resuming -= StopPage_Resuming;
        }
        #endregion

        #region Search processing
        private void SearchButton_Click(object sender, RoutedEventArgs e) {
            TextBox searchField;
            ObservableCollection<Stop> foundStops = new ObservableCollection<Stop>();
            if(_searchById == true){
                int stopId;
                searchField = (TextBox)CommonFunctionsClass.FindChildControl<TextBox>(SearchSection, "SearchIdValue");
                if(int.TryParse(searchField.Text, out stopId)) {
                    Stop foundStop = LtcDataSource2.getStopById(stopId);
                    if(foundStop != null) {
                        foundStops.Add(foundStop);
                    }
                }
            } else {
                searchField = (TextBox)CommonFunctionsClass.FindChildControl<TextBox>(SearchSection, "SearchNameValue");
                if(searchField.Text.Trim() == "Ukraine"){
                    var isManualActivatedOld = Windows.Storage.ApplicationData.Current.LocalSettings.Values["isManualActivated"];
                    bool isManualActivatedNew = false;
                    if(isManualActivatedOld == null || !(bool)isManualActivatedOld) {
                        isManualActivatedNew = true;
                    }
                    Windows.Storage.ApplicationData.Current.LocalSettings.Values["isManualActivated"] = isManualActivatedNew;

                    _fillProducts();
                } else if(searchField.Text.Length > 0) {
                    foundStops = LtcDataSource2.findStopsByName(searchField.Text.Trim());
                }
            }

            TextBlock ErrorTb = (TextBlock)CommonFunctionsClass.FindChildControl<TextBlock>(SearchSection, "ErrorTb");
            ListView SearchList = (ListView)CommonFunctionsClass.FindChildControl<ListView>(SearchSection, "SearchList");

            if(foundStops.Count > 0){
                ViewModel["FoundStops"] = foundStops;

                ErrorTb.Visibility = Visibility.Collapsed;
                SearchList.Visibility = Visibility.Visible;
            } else {
                ((ObservableCollection<Stop>)ViewModel["FoundStops"]).Clear();

                ErrorTb.Visibility = Visibility.Visible;
                SearchList.Visibility = Visibility.Collapsed;
            }
        }

        private void SearchOption_Checked(object sender, RoutedEventArgs e) {
            RadioButton _sender = (RadioButton)sender;
            TextBox tbIdSearch = (TextBox)CommonFunctionsClass.FindChildControl<TextBox>(SearchSection, "SearchIdValue");
            TextBox tbNameSearch = (TextBox)CommonFunctionsClass.FindChildControl<TextBox>(SearchSection, "SearchNameValue");
            if(tbIdSearch != null && tbNameSearch != null) {
                if(_sender.Name == "ById") {
                    ViewModel["SearchById"] = true;
                    _searchById = true;
                } else {
                    ViewModel["SearchById"] = false;
                    _searchById = false;
                }
                tbIdSearch.Text = "";
                tbNameSearch.Text = "";
            }
        }

        private void SearchRouteValue_TextChanged(object sender, TextChangedEventArgs e) {
            this.ViewModel["RoutesList"] = LtcDataSource2.getFilteredByIdRoutes(((TextBox)sender).Text);
        }
        #endregion

        #region Settings handlers
        private async void MailToButton_Click(object sender, RoutedEventArgs e) {
            await Windows.System.Launcher.LaunchUriAsync(new Uri("mailto:alextymofeev@gmail.com"));
        }

        private async void TileTypeSwitch_Toggled(object sender, RoutedEventArgs e) {
            ToggleSwitch _sender = (ToggleSwitch)sender;
            bool value = false;
            if(_sender.IsOn) {
                value = true;
            } else {
                value = false;
            }

            Windows.Storage.ApplicationData.Current.LocalSettings.Values["tileWithImage"] = value;
            ViewModel["tileWithImage"] = value;

            List<RouteStopLink> tiles = LtcDataSource2.getTiles();
            foreach(RouteStopLink tile in tiles) {
                try {
                    List<ScheduleItem> Schedule = await SUpdater.getScheduleByStopAndRoute3(tile);
                    if(Schedule != null){
                        SUpdater.UpdateSecondaryTile(Schedule, tile);
                    }
                } catch {
                }
            }
        }

        private async void PurchaseProductButton_Click(object sender, RoutedEventArgs e) {
            try {
#if DEBUG
                await CurrentAppSimulator.RequestProductPurchaseAsync(((Button)sender).Tag.ToString());
#else
                await CurrentApp.RequestProductPurchaseAsync(((Button)sender).Tag.ToString());
#endif
            } catch {
                this.ViewModel["StatusText"] = "Can't purchase product";
            }
            _fillProducts();
        }
        #endregion

        #region Favorites and Tiles processing
        private void FavoriteSettingsButton_Click(object sender, RoutedEventArgs e) {
            ScheduledTile selTile = (ScheduledTile)((Button)sender).DataContext;
            _canUpdateTile = false;
            FavoriteSwitch.IsOn = selTile.IsFavorite;
            TileSwitch.IsOn = selTile.IsTile;
            StopSettingsPopup.DataContext = selTile;
            _canUpdateTile = true;
            ViewModel["canAddTile"] = (Application.Current as App).canAddTile(selTile);
            StopSettingsPopup.IsOpen = true;
        }

        private void StopSettingsPopup_Close(object sender, RoutedEventArgs e) {
            StopSettingsPopup.IsOpen = false;
        }

        private void FavoriteSwitch_Toggled(object sender, RoutedEventArgs e) {
            if(_canUpdateTile) {
                ToggleSwitch _sender = (ToggleSwitch)sender;
                ScheduledTile selRoute = (ScheduledTile)_sender.DataContext;
                LtcDataSource2.toogleFavorite(selRoute, _sender.IsOn);
                if(!_sender.IsOn) {
                    ObservableCollection<ScheduledTile> favoritesList = (ObservableCollection<ScheduledTile>)ViewModel["FavoritesList"];
                    favoritesList.Remove(selRoute);
                    StopSettingsPopup.IsOpen = false;
                }
                ViewModel["canAddTile"] = (Application.Current as App).canAddTile(selRoute);
            }
        }

        private async void TileSwitch_Toggled(object sender, RoutedEventArgs e) {
            if(_canUpdateTile) {
                ToggleSwitch _sender = (ToggleSwitch)sender;
                ScheduledTile selRoute = (ScheduledTile)_sender.DataContext;
                if(!selRoute.IsTile && (Application.Current as App).canAddTile(selRoute)) {
                    ((App)Application.Current).Suspending += StopPage_Suspending;
                    ((App)Application.Current).Resuming += StopPage_Resuming;
                }
                await LtcDataSource2.toogleTile(selRoute, _sender.IsOn);
            }
        }
        #endregion

        #region Update Schedule
        private void UpdateScheduleButton_Click(object sender, RoutedEventArgs e) {
            _updateSchedule();
        }

        private async void _updateSchedule() {
            if((bool)ViewModel["isReady"]) {
                ViewModel["isReady"] = false;
                Loader.IsActive = true;
                ViewModel["ConnectionWarning"] = "Updating... Please wait.";
                Constants.connectionError cError = Constants.connectionError.ok;
                ObservableCollection<ScheduledTile> favoritesList = (ObservableCollection<ScheduledTile>)ViewModel["FavoritesList"];
                if(favoritesList.Count != 0) {
                    oldSchedule.Clear();
                    try {
                        for(int i = 0; i < favoritesList.Count; i++) {
                            if(_cancelToken) {
                                _cancelToken = false;
                                ViewModel["isReady"] = true;
                                Loader.IsActive = false;
                                return;
                            }
                            ScheduledTile link = favoritesList[i];
                            List<ScheduleItem> tmp = await SUpdater.getScheduleByStopAndRoute3(link);
                            if(tmp != null) {
                                if(tmp.FirstOrDefault() != null) {
                                    ScheduleItem tmpItem = tmp.FirstOrDefault();
                                    link.Time = tmpItem.Time;
                                    oldSchedule.Add(tmpItem);
                                } else {
                                    link.Time = null;
                                }
                                favoritesList[i] = link;

                                if(LtcDataSource2.isTile(link)) {
                                    SUpdater.UpdateSecondaryTile(tmp.OrderBy(r => r.TimeDif).ToList(), link);
                                }
                            } else {
                                cError = Constants.connectionError.siteNotAvailable;
                            }
                        }
                    } catch {
                        cError = Constants.connectionError.connectionFail;
                    }
                } else {
                    cError = Constants.connectionError.noData;
                }

                switch(cError) {
                    case Constants.connectionError.siteNotAvailable:
                        ViewModel["ConnectionWarning"] = "Ltc website is not available.";
                        break;
                    case Constants.connectionError.connectionFail:
                        ViewModel["ConnectionWarning"] = "Problems with Internet. Please try again later.";
                        break;
                    case Constants.connectionError.noData:
                        ViewModel["ConnectionWarning"] = "No Favorites found";
                        break;
                    case Constants.connectionError.ok:
                    default:
                        ViewModel["ConnectionWarning"] = "Last updated at: " + DateTime.Now.ToString("hh:mm:ss tt");
                        break;
                }

                Loader.IsActive = false;
                ViewModel["isReady"] = true;
            }
        }
        #endregion
    }
}