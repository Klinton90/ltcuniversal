﻿using LtcUniversal.Common;
using ScheduleUpdater;
using System;
using System.Collections.Generic;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using WPGeneralLib;
using ScheduleUpdater.DataModel;
using System.Threading.Tasks;

namespace LtcUniversal {
    public sealed partial class StopPage : Page
    {
        #region Standard params
        private readonly NavigationHelper navigationHelper;
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        private readonly ObservableDictionary _viewModel = new ObservableDictionary();
        public ObservableDictionary ViewModel
        {
            get { return this._viewModel; }
        }
        #endregion

        private bool _canUpdateTile = false;

        private static int oldStopId = 0;

        private static List<ScheduleItem> oldSchedule = new List<ScheduleItem>();

        private bool _cancelToken = false;

        public StopPage() {
            ViewModel["ConnectionWarning"] = "Press 'Update' to get times";
            ViewModel["Schedule"] = new List<ScheduleItem>();
            ViewModel["canAddTile"] = false;

            ViewModel["isReady"] = false;
            this.InitializeComponent();
            ViewModel["isReady"] = true;

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        private async void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            int adHeight = 0;
            if(!(Application.Current as App).checkIAP("noAds")) {
                var adv = AdCreator.createAdControl();
                Grid.SetRow(adv, 5);
                LayoutRoot.Children.Add(adv);
                adHeight = Constants.AdHeight;
            }

            string[] w = ((string)e.NavigationParameter).Split('_');
            Stop stop = LtcDataSource2.getStopById(int.Parse(w[0]));
            ViewModel["Stop"] = stop;
            ViewModel["AppHeight"] = Window.Current.Bounds.Height - Windows.UI.ViewManagement.StatusBar.GetForCurrentView().OccludedRect.Height - adHeight;
            ViewModel["AppWidth"] = Window.Current.Bounds.Width;
            ViewModel["RoutesList"] = LtcDataSource2.getRoutesWithDeskByStop(stop.StopId);

            if(oldSchedule.Count == 0 || oldStopId != stop.StopId) {
                await _updateSchedule();
            } else {
                ViewModel["Schedule"] = oldSchedule;
            }
            oldStopId = stop.StopId;
        }
        
        #region NavigationHelper registration

        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e) {
            // TODO: Save the unique state of the page here.
        }

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            _cancelToken = true;
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        #region Update Schedule

        private async void UpdateButton_Click(object sender, RoutedEventArgs e) {
            await _updateSchedule();
        }

        private async Task _updateSchedule() {
            if((bool)ViewModel["isReady"]) {
                ViewModel["isReady"] = false;
                ViewModel["ConnectionWarning"] = "Updating... Please wait.";
                Constants.connectionError cError = Constants.connectionError.ok;
                ((List<ScheduleItem>)ViewModel["Schedule"]).Clear();

                try {
                    List<Tile> routesList = (List<Tile>)ViewModel["RoutesList"];
                    foreach(Tile link in routesList) {
                        if(_cancelToken) {
                            _cancelToken = false;
                            return;
                        }
                        List<ScheduleItem> tmp = await SUpdater.getScheduleByStopAndRoute3(link);
                        if(tmp != null){
                            ViewModel["Schedule"] = ((List<ScheduleItem>)ViewModel["Schedule"]).Concat(tmp).OrderBy(r => r.TimeDif).ToList();

                            if(LtcDataSource2.isTile(link)) {
                                SUpdater.UpdateSecondaryTile(tmp.OrderBy(r => r.TimeDif).ToList(), link);
                            }
                        }else{
                            cError = Constants.connectionError.siteNotAvailable;
                        }
                    }
                } catch {
                    cError = Constants.connectionError.connectionFail;
                }

                if(((List<ScheduleItem>)ViewModel["Schedule"]).Count == 0) {
                    ViewModel["ConnectionWarning"] = "No Buses found. Try again later.";
                } else if(cError == Constants.connectionError.siteNotAvailable) {
                    ViewModel["ConnectionWarning"] = "Ltc website is not available.";
                } else if(cError == Constants.connectionError.connectionFail){
                    ViewModel["ConnectionWarning"] = "Problems with Internet. Please try again later.";
                } else {
                    ViewModel["ConnectionWarning"] = "Last updated at: " + DateTime.Now.ToString("hh:mm:ss tt");
                    oldSchedule = (List<ScheduleItem>)ViewModel["Schedule"];
                }

                ViewModel["isReady"] = true;
            }
        }

        #endregion

        #region Favorite and Tile processing

        private void SelectRoutePopup_Close(object sender, RoutedEventArgs e){
            SelectRoutePopup.IsOpen = false;
        }

        private void StopSettingsPopup_Close(object sender, RoutedEventArgs e) {
            StopSettingsPopup.IsOpen = false;
        }

        private void FavoriteSwitch_Toggled(object sender, RoutedEventArgs e) {
            if(_canUpdateTile) {
                ToggleSwitch _sender = (ToggleSwitch)sender;
                Tile selRoute = (Tile)_sender.DataContext;
                LtcDataSource2.toogleFavorite(selRoute, _sender.IsOn);
                ViewModel["canAddTile"] = (Application.Current as App).canAddTile(selRoute);
            }
        }

        private async void TileSwitch_Toggled(object sender, RoutedEventArgs e) {
            if(_canUpdateTile){
                ToggleSwitch _sender = (ToggleSwitch)sender;
                Tile selRoute = (Tile)_sender.DataContext;
                bool canAddTile = (Application.Current as App).canAddTile(selRoute);
                ViewModel["canAddTile"] = canAddTile;
                if (!selRoute.IsTile && canAddTile) {
                    ((App)Application.Current).Suspending += StopPage_Suspending;
                }
                await LtcDataSource2.toogleTile(selRoute, _sender.IsOn);
            }
        }
        
        private void FavoriteButton_Click(object sender, RoutedEventArgs e) {
            List<Tile> routesList = (List<Tile>)ViewModel["RoutesList"];
            if (routesList.Count == 1){
                _showSettingsPopup(routesList[0]);
            }else{
                SelectRoutePopup.IsOpen = true;
            }
        }

        private void Route_Click(object sender, ItemClickEventArgs e) {
            _showSettingsPopup((Tile)e.ClickedItem);
        }

        private void _showSettingsPopup(Tile selRoute) {
            _canUpdateTile = false;
            FavoriteSwitch.IsOn = selRoute.IsFavorite;
            TileSwitch.IsOn = selRoute.IsTile;
            StopSettingsPopup.DataContext = selRoute;
            _canUpdateTile = true;
            
            ViewModel["canAddTile"] = (Application.Current as App).canAddTile(selRoute);
            StopSettingsPopup.IsOpen = true;
        }

        #endregion

        #region Navigation
        void StopPage_Suspending(object sender, Windows.ApplicationModel.SuspendingEventArgs e) {
            Tile selRoute = (Tile)((ToggleSwitch)TileSwitch).DataContext;
            List<ScheduleItem> sList = (List<ScheduleItem>)ViewModel["Schedule"];
            var tList = from s in sList
                        where s.RouteId == selRoute.RouteId && s.StopId == selRoute.StopId
                        select new ScheduleData { Time = s.Time };
            SUpdater.UpdateSecondaryTile(tList, selRoute);

            (Application.Current as App).Suspending -= StopPage_Suspending;
        }

        private void GoHome_Click(object sender, RoutedEventArgs e) {
            if(!Frame.Navigate(typeof(TopPage))) {
                throw new Exception("NavigationFailedExceptionMessage");
            }
        }

        #endregion
    }
}