﻿using ScheduleUpdater.DataModel;
using LtcUniversal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Graphics.Display;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;
using WPGeneralLib;
using ScheduleUpdater;
using Windows.UI.Xaml.Media.Imaging;
using Windows.ApplicationModel;

namespace LtcUniversal {
    public sealed partial class MapPage : Page
    {
        private RouteDb route;

        private List<ExtendedStop> routeStops;

        private Geolocator locator;

        private MapIcon UserPosition;

        CoreDispatcher dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;

        private bool _visibleStops = false;

        private bool _firstPositionFound = false;

        private bool _cancelToken = false;

        private static bool _initialLoad = true;

        private bool _stopMapLoading = false;

        private Canvas cloud = new Canvas() {
            Visibility = Visibility.Collapsed,
            Width = 300,
            Height = 310
        };

        Button goToStopButton = new Button() {
            Content = "Go to Stop's page",
            MinHeight = 20,
            Foreground = new SolidColorBrush(Colors.Black),
            Margin = new Thickness(0),
            BorderBrush = new SolidColorBrush(Colors.Black),
            BorderThickness = new Thickness(1),
            FontSize = 13,
            Width = 300
        };

        private Geopoint positionCenter = new Geopoint(new BasicGeoposition() {
            Latitude = 42.9862,
            Longitude = -081.2438
        });

        #region Standard params
        private readonly NavigationHelper navigationHelper;
        public NavigationHelper NavigationHelper {
            get {
                return this.navigationHelper;
            }
        }

        private ObservableDictionary _viewModel = new ObservableDictionary();
        public ObservableDictionary ViewModel {
            get {
                return this._viewModel;
            }
        }
        #endregion

        public MapPage() {
            this.InitializeComponent();
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait;

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
            ((App)Application.Current).Suspending += MapPage_Suspending;

            _createCloud();
        }

        private void MapPage_Suspending(object sender, SuspendingEventArgs e) {
            (Application.Current as App).Suspending -= MapPage_Suspending;
            _initialLoad = false;
        }

        private async void NavigationHelper_LoadState(object sender, LoadStateEventArgs e) {
            if(!_initialLoad) {
                _stopMapLoading = true;
                _initialLoad = true;
                Frame.GoBack();
                return;
            }
            this.ViewModel["ShowProcessing"] = true;
            ViewModel["ShowUserPosition"] = false;

            await Windows.UI.ViewManagement.StatusBar.GetForCurrentView().HideAsync();

            if(!(Application.Current as App).checkIAP("noAds")) {
                var adv = AdCreator.createAdControl();
                Grid.SetRow(adv, 1);
                LayoutRoot.Children.Add(adv);
            }

            if(e.NavigationParameter != null) {
                route = LtcDataSource2.getRouteById(Convert.ToInt32(e.NavigationParameter));
                routeStops = LtcDataSource2.getStopsForMap(route.RouteId);
                MapHelp.Text = "Zoom in to see more stops";
            }

            locator = new Geolocator() {
                DesiredAccuracyInMeters = 50,
                ReportInterval = 30000,
                MovementThreshold = 10
            };
            locator.StatusChanged += locator_StatusChanged;
            locator.PositionChanged += locator_PositionChanged;
        }

        #region NavigationHelper registration

        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e) {
            // TODO: Save the unique state of the page here.
        }

        protected override void OnNavigatedTo(NavigationEventArgs e) {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected async override void OnNavigatedFrom(NavigationEventArgs e) {
            _cancelToken = true;
            if(_initialLoad) {
                this.ViewModel["ShowProcessing"] = true;

                MapError.DataContext = null;
                locator = null;
                dispatcher = null;
                var ed = RouteMap.Children.Where(x => x.GetType() == typeof(Ellipse)).ToList();
                foreach(Ellipse fence in ed) {
                    fence.Tapped -= _stopTapped;
                }
                RouteMap.Children.Clear();
                RouteMap.MapElements.Clear();
                RouteMap = null;
                _viewModel = null;
                routeStops = null;
                this.DataContext = null;

                System.GC.Collect(3);
                System.GC.WaitForPendingFinalizers();
                System.GC.Collect(3);

                await Windows.UI.ViewManagement.StatusBar.GetForCurrentView().ShowAsync();
            }

            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        #region Locator functions
        private void locator_PositionChanged(Geolocator sender, PositionChangedEventArgs args) {
            _showUserPostition(args.Position);
        }

        private void locator_StatusChanged(Geolocator sender, StatusChangedEventArgs args) {
            if(args.Status != PositionStatus.Disabled && args.Status != PositionStatus.NotAvailable && args.Status != PositionStatus.NoData) {
                ViewModel["ShowUserPosition"] = true;
            } else {
                ViewModel["ShowUserPosition"] = false;
            }
        }

        private async void _showUserPostition(Geoposition position) {
            await dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () => {
                if(UserPosition != null) {
                    RouteMap.MapElements.Remove(UserPosition);
                }
                UserPosition = new MapIcon() {
                    Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/Center_direction_small.png")),
                    Location = position.Coordinate.Point,
                    NormalizedAnchorPoint = new Point(0.5,0.5)
                };
                RouteMap.MapElements.Add(UserPosition);

                if(route == null){
                    List<ExtendedStop> Stops = LtcDataSource2.getNearestStops(position.Coordinate);

                    var ed = RouteMap.Children.Where(x => x.GetType() == typeof(Ellipse)).ToList();
                    foreach(Ellipse fence in ed) {
                        fence.Tapped -= _stopTapped;
                        RouteMap.Children.Remove(fence);
                    }

                    List<BasicGeoposition> glist = _addStopsToMap(Stops);

                    if(!_firstPositionFound) {
                        _firstPositionFound = true;
                        glist.Add(position.Coordinate.Point.Position);
                        await RouteMap.TrySetViewBoundsAsync(GeoboundingBox.TryCompute(glist), new Thickness(2), MapAnimationKind.Default);
                    }
                }
            });
        }
        #endregion

        #region Cloud handlers
        private void _createCloud() {
            PointCollection PointCollection = new PointCollection();
            PointCollection.Add(new Point(0, 0));
            PointCollection.Add(new Point(300, 0));
            PointCollection.Add(new Point(300, 280));
            PointCollection.Add(new Point(180, 280));
            PointCollection.Add(new Point(150, 310));
            PointCollection.Add(new Point(120, 280));
            PointCollection.Add(new Point(0, 280));
            Polygon cloudShape = new Polygon() {
                Stroke = new SolidColorBrush(Colors.Black),
                Fill = new SolidColorBrush(Colors.LightSeaGreen),
                StrokeThickness = 2,
                Points = PointCollection,
                Opacity = 0.5,
            };

            Line lineTop = new Line() {
                X1 = 0,
                X2 = 270,
                Y1 = 20,
                Y2 = 20,
                Stroke = new SolidColorBrush(Colors.Black),
                StrokeThickness = 1,
            };

            ImageBrush closeButtonPicture = new ImageBrush() {
                ImageSource = new BitmapImage(new Uri(Constants.CloseBrush))
            };
            Button closeCloudButton = new Button() {
                Background = closeButtonPicture,
                MinHeight = 40,
                MinWidth = 20,
                BorderThickness = new Thickness(0)
            };
            closeCloudButton.Click += closeCloudButton_Click;
            Canvas.SetLeft(closeCloudButton, 270);
            Canvas.SetZIndex(closeCloudButton, 10000);

            goToStopButton.Click += goToStopButton_Click;
            Canvas.SetTop(goToStopButton, 250);

            cloud.Children.Add(cloudShape);
            cloud.Children.Add(closeCloudButton);
            cloud.Children.Add(goToStopButton);
            cloud.Children.Add(lineTop);

            RouteMap.Children.Add(cloud);
        }

        void closeCloudButton_Click(object sender, RoutedEventArgs e) {
            cloud.Visibility = Visibility.Collapsed;
        }

        private TextBlock _createTextInCloud(string message, int y_position, bool mono = false) {
            TextBlock tb = new TextBlock() {
                Height = 20,
                Width = 290,
                FontSize = 14,
                Foreground = new SolidColorBrush(Colors.Black),
                Text = message,
                FontFamily = mono ? new FontFamily("Courier New") : new FontFamily("Segoe UI")
            };
            Canvas.SetTop(tb, y_position);
            Canvas.SetLeft(tb, 5);

            cloud.Children.Add(tb);
            return tb;
        }
        #endregion

        #region Navigation to other pages
        void goToStopButton_Click(object sender, RoutedEventArgs e) {
            Button _sender = (Button)sender;
            if(_sender.Tag != null) {
                if(!Frame.Navigate(typeof(StopPage), _sender.Tag.ToString())) {
                    throw new Exception("NavigationFailedExceptionMessage");
                }
            }
        }
        #endregion

        #region Map handlers

        private async void RouteMap_Loaded(object sender, RoutedEventArgs e) {
            if(!_stopMapLoading) {
                navigationHelper.PreventGoBack = true;

                if(route != null) {
                    List<List<BasicGeoposition>> mapData = new List<List<BasicGeoposition>>();
                    string[] subLines = route.MapData.Substring(1, route.MapData.Length - 5).Split('|');
                    foreach(string rawSubLine in subLines) {
                        List<BasicGeoposition> positionsList = new List<BasicGeoposition>();

                        string[] subLine = rawSubLine.Split('*');
                        string[] rawCoordinates = subLine[0].Split(';');
                        foreach(string RawCoordinate in rawCoordinates) {
                            string[] Coordinates = RawCoordinate.Split(' ');

                            BasicGeoposition position = new BasicGeoposition();
                            position.Latitude = Convert.ToDouble(Coordinates[1]);
                            position.Longitude = Convert.ToDouble(Coordinates[0]);

                            positionsList.Add(position);
                        }

                        mapData.Add(positionsList);
                    }

                    foreach(List<BasicGeoposition> line in mapData) {
                        Geopath GeoPath = new Geopath(line);
                        MapPolyline RouteLine = new MapPolyline();
                        RouteLine.Path = GeoPath;
                        RouteLine.StrokeThickness = 3;
                        RouteMap.MapElements.Add(RouteLine);
                    }

                    List<BasicGeoposition> glist = _addStopsToMap(routeStops);

                    this.ViewModel["ShowProcessing"] = false;
                    await RouteMap.TrySetViewBoundsAsync(GeoboundingBox.TryCompute(glist), new Thickness(2), MapAnimationKind.Default);
                } else {
                    this.ViewModel["ShowProcessing"] = false;
                    await RouteMap.TrySetViewAsync(positionCenter, 12D);
                }

                if(locator.LocationStatus != PositionStatus.Disabled && locator.LocationStatus != PositionStatus.NotAvailable && locator.LocationStatus != PositionStatus.NoData) {
                    Geoposition position = await locator.GetGeopositionAsync(new TimeSpan(0, 5, 0), new TimeSpan(0, 0, 60));
                    if(position != null) {
                        _showUserPostition(position);
                    } else {
                        ViewModel["ShowUserPosition"] = false;
                    }
                } else {
                    ViewModel["ShowUserPosition"] = false;
                }
                navigationHelper.PreventGoBack = false;
            }
        }

        private List<BasicGeoposition> _addStopsToMap(List<ExtendedStop> Stops) {
            List<BasicGeoposition> glist = new List<BasicGeoposition>();

            foreach(ExtendedStop stop in Stops) {
                Visibility vis = Visibility.Visible;
                Color colFill = Colors.Violet;
                Color colStroke = Colors.Black;

                BasicGeoposition position = new BasicGeoposition() {
                    Latitude = stop.Lat,
                    Longitude = stop.Lng
                };
                glist.Add(position);

                if(route != null){
                    if(stop.IsMajor != true) {
                        vis = Visibility.Collapsed;
                        colFill = Colors.Green;
                    }
                    switch(stop.Direction){
                        case Direction.North:
                            colStroke = Colors.Blue;
                            break;
                        case Direction.South:
                            colStroke = Colors.Red;
                            break;
                        case Direction.East:
                            colStroke = Colors.Orange;
                            break;
                        case Direction.West:
                            colStroke = Colors.Magenta;
                            break;
                    }
                }

                Ellipse fence = new Ellipse() {
                    Width = 30,
                    Height = 30,
                    Fill = new SolidColorBrush(colFill),
                    DataContext = stop,
                    Visibility = vis,
                    Opacity = 0.3,
                    Stroke = new SolidColorBrush(colStroke),
                    StrokeThickness = 5
                };
                fence.Tapped += _stopTapped;

                RouteMap.Children.Add(fence);
                MapControl.SetLocation(fence, new Geopoint(position));
                MapControl.SetNormalizedAnchorPoint(fence, new Point(0.5, 0.5));
            }

            return glist;
        }

        private async void _stopTapped(object sender, TappedRoutedEventArgs e) {
            ExtendedStop stop = (ExtendedStop)((Ellipse)sender).DataContext;
            List<ScheduleItem> schedule = new List<ScheduleItem>();
            List<TextBlock> oldSchedule = new List<TextBlock>();
            Constants.connectionError cError = Constants.connectionError.ok;
            string errorMessage = String.Empty;
            goToStopButton.Tag = stop.StopId;

            this.ViewModel["ShowProcessing"] = true;

            var oldPopupEls = cloud.Children.Where(x => x.GetType() == typeof(TextBlock)).ToList();
            foreach(TextBlock tbd in oldPopupEls) {
                cloud.Children.Remove(tbd);
            }
            _createTextInCloud(stop.StopId.ToString() + ": " + stop.Name, 0);
            TextBlock statusTextBlock = _createTextInCloud("Updating... Please wait.", 20);


            BasicGeoposition position = new BasicGeoposition() {
                Latitude = stop.Lat,
                Longitude = stop.Lng
            };
            Geopoint point = new Geopoint(position);

            MapControl.SetLocation(cloud, point);
            MapControl.SetNormalizedAnchorPoint(cloud, new Point(0.5, 1));
            cloud.Visibility = Visibility.Visible;
            await RouteMap.TrySetViewAsync(point, Math.Max(RouteMap.ZoomLevel, 12D));

            List<RouteStopLink> links = LtcDataSource2.getRoutesByStop(stop.StopId);
            try {
                foreach(RouteStopLink link in links) {
                    if(_cancelToken) {
                        return;
                    }
                    List<ScheduleItem> tmp = await SUpdater.getScheduleByStopAndRoute3(link);
                    if(tmp != null) {
                        schedule = schedule.Concat(tmp).OrderBy(r => r.TimeDif).ToList();

                        foreach(TextBlock tb in oldSchedule) {
                            cloud.Children.Remove(tb);
                        }

                        int i = 2;
                        foreach(ScheduleItem item in schedule) {
                            string message = String.Format("{0,2} {1} {2}", item.RouteId.ToString(), item.TimeString, item.DirectionText);
                            oldSchedule.Add(_createTextInCloud(message, 20 * i, true));
                            if(i++ == 12) {
                                break;
                            }
                        }

                        if(LtcDataSource2.isTile(link)) {
                            SUpdater.UpdateSecondaryTile(tmp.OrderBy(r => r.TimeDif).ToList(), link);
                        }
                    } else {
                        cError = Constants.connectionError.siteNotAvailable;
                    }
                }
            } catch {
                cError = Constants.connectionError.connectionFail;
            }

            if(schedule.Count == 0) {
                errorMessage = "No Buses found. Try again later.";
            } else if(cError == Constants.connectionError.siteNotAvailable) {
                errorMessage = "Ltc website is not available.";
            } else if(cError == Constants.connectionError.connectionFail) {
                errorMessage = "Problems with Internet. Please try again later.";
            } else {
                errorMessage = "Last updated at: " + DateTime.Now.ToString("hh:mm:ss tt");
            }

            cloud.Children.Remove(statusTextBlock);
            _createTextInCloud(errorMessage, 20);
            this.ViewModel["ShowProcessing"] = false;
        }

        #endregion

        #region MapEvents

        private void RouteMap_ZoomLevelChanged(MapControl sender, object args) {
            if(route != null) {
                if(sender.ZoomLevel > 15) {
                    if(_visibleStops != true) {
                        foreach(Ellipse fence in sender.Children.Where(e => e.GetType() == typeof(Ellipse))) {
                            fence.Visibility = Visibility.Visible;
                        }
                        _visibleStops = true;
                    }
                    MapHelp.Text = "Tap on stop to see schedule";
                } else if(sender.ZoomLevel < 15) {
                    if(_visibleStops == true) {
                        foreach(Ellipse fence in sender.Children.Where(e => e.GetType() == typeof(Ellipse))) {
                            if(((ExtendedStop)fence.DataContext).IsMajor != true) {
                                fence.Visibility = Visibility.Collapsed;
                            }
                        }
                        _visibleStops = false;
                    }
                    MapHelp.Text = "Zoom in to see more stops";
                }
            }
        }

        private async void ShowUserPosition_Click(object sender, RoutedEventArgs e) {
            if(UserPosition != null){
                await RouteMap.TrySetViewAsync(UserPosition.Location, 15.1);
            }
        }

        #endregion

        #region compute center betweem geopoints
        private Geopoint _findCenterPosForUserLoc(List<Stop> locs, Geocoordinate userPos) {
            double maxX = 0;
            double minX = 360;
            double maxY = 0;
            double minY = 360;
            locs.Add(new Stop() {
                Lat = userPos.Point.Position.Latitude,
                Lng = userPos.Point.Position.Longitude
            });

            foreach(Stop loc in locs) {
                if(Math.Abs(maxX) < Math.Abs(loc.Lat)) {
                    maxX = loc.Lat;
                }
                if(Math.Abs(minX) > Math.Abs(loc.Lat)) {
                    minX = loc.Lat;
                }
                if(Math.Abs(maxY) < Math.Abs(loc.Lng)) {
                    maxY = loc.Lng;
                }
                if(Math.Abs(minY) > Math.Abs(loc.Lng)) {
                    minY = loc.Lng;
                }
            }

            return new Geopoint(new BasicGeoposition() {
                Latitude = minX + (maxX - minX) / 2,
                Longitude = minY + (maxY - minY) / 2
            });
        }
        #endregion
    }
}
