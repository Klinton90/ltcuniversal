﻿using ScheduleUpdater.DataModel;
using System;
using System.Collections.Generic;
using WPGeneralLib;

namespace LtcUniversal {
    public class DummyData
    {
        private readonly ObservableDictionary _viewModel = new ObservableDictionary();
        public ObservableDictionary ViewModel {
            get {
                return this._viewModel;
            }
        }

        public ObservableDictionary ViewModel2 {
            get {
                var q = new DummyData();
                return q.ViewModel;
            }
        }

        public double AppWidth = 480;

        public double AppHeight = 800;

        public RouteDb route = new RouteDb() {RouteId = 91, RouteName = "TestRoute"};

        public Stop stop = new Stop() {StopId = 3333,Name = "Test Stop Test Stop Test Stop Test Stop"};

        public List<Direction> Directions = new List<Direction>();

        public List<Stop> Stops_Dir0 = new List<Stop>();

        public List<Stop> Stops_Dir1 = new List<Stop>();

        public List<ScheduledTile> FavoritesList = new List<ScheduledTile>();

        public List<Stop> FoundStops = new List<Stop>();

        public List<ExtendedStop> TilesList = new List<ExtendedStop>();

        public List<RouteDb> RoutesList = new List<RouteDb>();

        public List<ScheduleItem> Schedule = new List<ScheduleItem>();

        public DummyData() {
            Stops_Dir0.Add(new Stop() {StopId = 1,Name = "Stop1 Long Long Long Name Long Long Long Name "/*,Direction = Direction.East,IsMajor = true, Lat = 1.01, Lng = 2.01*/});
            Stops_Dir0.Add(new Stop() {StopId = 2,Name = "Stop2"/*,Direction = Direction.East,IsMajor = true,Lat = 1.01,Lng = 2.01*/});
            Stops_Dir0.Add(new Stop() {StopId = 3000,Name = "Stop3"/*,Direction = Direction.East,IsMajor = true,Lat = 1.01,Lng = 2.01*/});
            Stops_Dir1.Add(new Stop() {StopId = 11,Name = "Stop11"/*,Direction = Direction.West,IsMajor = true, Lat = 1.01, Lng = 2.01*/});
            Stops_Dir1.Add(new Stop() {StopId = 21,Name = "Stop21"/*,Direction = Direction.West,IsMajor = true,Lat = 1.01,Lng = 2.01*/});
            Stops_Dir1.Add(new Stop() {StopId = 31,Name = "Stop31"/*,Direction = Direction.West,IsMajor = true,Lat = 1.01,Lng = 2.01*/});
            FavoritesList.Add(new ScheduledTile() {StopId = 11,Name = "Fav11 sddfg dfg dfg dfg dfg dfg dfg dfg dfdfgdf ", RouteId = 1, Time = new TimeSpan(123)/*,Direction = Direction.West,IsMajor = true, Lat = 1.01, Lng = 2.01*/});
            FavoritesList.Add(new ScheduledTile() {StopId = 21,Name = "Fav21", RouteId = 2, Time = new TimeSpan(234)/*,Direction = Direction.West,IsMajor = true,Lat = 1.01,Lng = 2.01*/});
            FavoritesList.Add(new ScheduledTile() {StopId = 3333,Name = "Fav31", RouteId = 33, Time = new TimeSpan(345)/*,Direction = Direction.West,IsMajor = true,Lat = 1.01,Lng = 2.01*/});
            TilesList.Add(new ExtendedStop() {StopId = 11,Name = "Fav11", RouteId=1, Direction = Direction.East});
            TilesList.Add(new ExtendedStop() {StopId = 21,Name = "Fav21", RouteId=32, Direction = Direction.North});
            TilesList.Add(new ExtendedStop() {StopId = 3325,Name = "Fav31", RouteId=16, Direction = Direction.South});
            FoundStops.Add(new Stop() {StopId = 11,Name = "Stop11"});
            FoundStops.Add(new Stop() {StopId = 21,Name = "Stop21"});
            FoundStops.Add(new Stop() {StopId = 3325,Name = "Stop3325 Long Long Long Name Even More Long Name"});
            RoutesList.Add(new RouteDb() {RouteId = 1, RouteName="Route1"});
            RoutesList.Add(new RouteDb() {RouteId = 2, RouteName="Route2"});
            RoutesList.Add(new RouteDb() {RouteId = 33, RouteName="Route3 Long Long Long Name Even More Long Name"});
            Schedule.Add(new ScheduleItem(){Time = new TimeSpan(2, 52, 0), RouteId = 1, DirectionText="TO Kipps Lane at Briarhill TO Kipps Lane at Briarhill"});
            Schedule.Add(new ScheduleItem(){Time = new TimeSpan(12, 52, 0), RouteId = 90, DirectionText="TO Kipps Lane at Briarhill"});
            Schedule.Add(new ScheduleItem(){Time = new TimeSpan(12, 52, 0), RouteId = 32, DirectionText="TO Kipps Lane at Briarhill"});
            Directions.Add(Direction.East);
            Directions.Add(Direction.West);

            this.ViewModel["Route"] = route;
            this.ViewModel["Stop"] = stop;
            this.ViewModel["Directions"] = Directions;
            this.ViewModel["Stops_Dir1"] = Stops_Dir0;
            this.ViewModel["Stops_Dir2"] = Stops_Dir1;
            this.ViewModel["RoutesList"] = RoutesList;
            this.ViewModel["FoundStops"] = FoundStops;
            this.ViewModel["Schedule"] = Schedule;
            this.ViewModel["FavoritesList"] = FavoritesList;
            this.ViewModel["TilesList"] = TilesList;

            this.ViewModel["pinnedRouteId"] = 32;
            this.ViewModel["pinnedStopId"] = 1093;
            this.ViewModel["AppHeight"] = 400;
            this.ViewModel["AppWidth"] = 400;
            this.ViewModel["IsTrial"] = false;
            this.ViewModel["canAddTile"] = true;
            this.ViewModel["ConnectionWarning"] = "Please wait";
        }
    }
}
