﻿using Microsoft.Advertising.WinRT.UI;
using System;
using Windows.UI.Xaml;

namespace LtcUniversal {
    public class AdCreator
    {
        public static AdControl createAdControl(string _AdUnitId = "") {
            return new AdControl() {
                AdUnitId = _AdUnitId != String.Empty ? _AdUnitId : Constants.AdUnitId,
                ApplicationId = Constants.AdApplicationId,
                Height = Constants.AdHeight,
                Width = Constants.AdWidth,
                VerticalAlignment = VerticalAlignment.Top,
                HorizontalAlignment = HorizontalAlignment.Center,
                IsAutoRefreshEnabled = true,
            };
        }
    }
}
