﻿using ScheduleUpdater.DataModel;
using LtcUniversal.Common;
using ScheduleUpdater;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Store;
using Windows.Storage;
using Windows.UI.StartScreen;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using WPGeneralLib;

namespace LtcUniversal
{
    public sealed partial class App : Application
    {
        public LicenseInformation licenseInformation;

        public bool doNotRestore = false;

#if WINDOWS_PHONE_APP
        private TransitionCollection transitions;
#endif
        public App()
        {
            this.InitializeComponent();
            this.Suspending += this.OnSuspending;
            this.Resuming += App_Resuming;
        }

        public bool checkIAP(String productName) {
            var _isManualActivated = Windows.Storage.ApplicationData.Current.LocalSettings.Values["isManualActivated"];
            bool isManualActivated = _isManualActivated != null ? (bool)_isManualActivated : false;
            return licenseInformation.ProductLicenses[productName].IsActive || isManualActivated;
        }

        public bool canAddTile(ILink selRoute) {
            int tCount = LtcDataSource2.getTiles().Count;
            bool noAds = checkIAP("tiles");
            bool checkTrial = ((!noAds && tCount == 0) || (noAds && tCount < Constants.maxTiles));
            return ((checkTrial && selRoute.IsFavorite) || selRoute.IsTile);
        }

        private void checkTiles() {
            List<RouteStopLink> tiles = LtcDataSource2.getTiles();
            foreach(RouteStopLink tile in tiles) {
                if(!SecondaryTile.Exists(SUpdater.getNavStringForStopPage(tile))) {
                    tile.IsTile = false;
                    LtcDataSource2.updateLink(tile, Constants.linkTableName);
                }
            }

            if(tiles.Count > 0) {
                if(!AppBackgroundTaskRegistrator.IsRegistered(Constants.taskName)) {
                    AppBackgroundTaskRegistrator.RegisterBackgroundTask(Constants.taskEntryPoint, Constants.taskName, Constants.triggers, Constants.conditions);
                }
            } else {
                AppBackgroundTaskRegistrator.UnregisterBackgroundTask(Constants.taskName);
            }
        }

        async void App_Resuming(object sender, object e) {
            checkTiles();
            if(!doNotRestore) {
                try {
                    await SuspensionManager.RestoreAsync();
                } catch(SuspensionManagerException) {
                    // Something went wrong restoring state.
                    // Assume there is no state and continue
                }
            } else {
                doNotRestore = false;
            }
        }

        protected async override void OnLaunched(LaunchActivatedEventArgs e)
        {
            var CurrentVersion = Windows.Storage.ApplicationData.Current.LocalSettings.Values["CurrentVersion"];
            var _version = Package.Current.Id.Version;
            string LaunchVersion = _version.Major.ToString() + "." + _version.Minor.ToString() + "." + _version.Build.ToString() + "." + _version.Revision.ToString();
            if(CurrentVersion != null && (string)CurrentVersion != LaunchVersion) {
                try {
                    List<RouteStopLink> tiles = LtcDataSource2.getTiles();
                    List<Tile> favorites = LtcDataSource2.getFavorites();
                    LtcDataSource2.restoreLtcData();
                    foreach(RouteStopLink tile in tiles) {
                        LtcDataSource2.updateLink(tile, "RouteStopLink");
                    }
                    foreach(Tile favorite in favorites) {
                        LtcDataSource2.updateLink(favorite, "RouteStopLink");
                    }
                } catch { }
            }
            Windows.Storage.ApplicationData.Current.LocalSettings.Values["CurrentVersion"] = LaunchVersion;

            if(e.Arguments != "") {
                doNotRestore = true;
            }

#if DEBUG
            WPFileOperations helper = new WPFileOperations(Constants.dataFolder);
            var appSimulatorStorageFile = await helper.getFileAsync(Constants.simFileName);
            await CurrentAppSimulator.ReloadSimulatorAsync(appSimulatorStorageFile);
            licenseInformation = CurrentAppSimulator.LicenseInformation;
#else
            licenseInformation = CurrentApp.LicenseInformation;
#endif
#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached)
            {
                this.DebugSettings.EnableFrameRateCounter = true;
            }
#endif

            Frame rootFrame = Window.Current.Content as Frame;

            if (rootFrame == null){
                rootFrame = new Frame();
                SuspensionManager.RegisterFrame(rootFrame, "AppFrame");
                rootFrame.CacheSize = 2;

                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated) {
                    if(doNotRestore) {
                        try {
                            await SuspensionManager.RestoreAsync();
                        } catch(SuspensionManagerException) {
                            // Something went wrong restoring state.
                            // Assume there is no state and continue
                        }
                    } else {
                        doNotRestore = false;
                    }
                }
                Window.Current.Content = rootFrame;
            }
            if(!(await ApplicationData.Current.LocalFolder.FileExists(Constants.dbName))) {
                WPFileOperations fileReader = new WPFileOperations(Constants.dataFolder);
                await fileReader.copyFileToLocalStorageAsync(Constants.dbName, Constants.dbName);
            }

            checkTiles();

            if(rootFrame.Content == null) {
#if WINDOWS_PHONE_APP
                if(rootFrame.ContentTransitions != null) {
                    this.transitions = new TransitionCollection();
                    foreach(var c in rootFrame.ContentTransitions) {
                        this.transitions.Add(c);
                    }
                }

                rootFrame.ContentTransitions = null;
                rootFrame.Navigated += this.RootFrame_FirstNavigated;
#endif
                if(!rootFrame.Navigate(typeof(TopPage))) {
                    throw new Exception("Failed to create initial page");
                }
            }

            if(e.Arguments != "") {
                if(!rootFrame.Navigate(typeof(StopPage), e.Arguments)) {
                    throw new Exception("Failed to create initial page");
                }
            }

            Window.Current.Activate();
        }

#if WINDOWS_PHONE_APP
        private void RootFrame_FirstNavigated(object sender, NavigationEventArgs e)
        {
            var rootFrame = sender as Frame;
            rootFrame.ContentTransitions = this.transitions ?? new TransitionCollection() { new NavigationThemeTransition() };
            rootFrame.Navigated -= this.RootFrame_FirstNavigated;
        }
#endif

        private async void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            await SuspensionManager.SaveAsync();
            deferral.Complete();
        }
    }
}